﻿using UDProject.Inputs;
using UnityEngine;
using System.Collections.Generic;

namespace UDProject.EditorUtility
{
	public static class StandardInputControllerInspectorUtility
	{
		public static void FillKeys(this StandardInputController controller, int length)
		{
			if (controller.keys == null)
			{
				controller.keys = new List<KeyCode> ();
			}
			if (controller.keys.Count != length) 
			{
				controller.keys = new List<KeyCode> ();
				while (controller.keys.Count < length) 
				{
					controller.keys.Add (KeyCode.None);
				}
			}
		}
	}
}

