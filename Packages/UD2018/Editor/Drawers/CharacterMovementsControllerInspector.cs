﻿using UDProject.Movements;
using UnityEditor;

namespace UDProject.EditorUtility
{
	[CustomEditor(typeof(CharacterMovementsController))]
	public class CharacterMovementsControllerInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			EditorGUILayout.LabelField ("1) Move forward");
			EditorGUILayout.LabelField ("2) Move backward");
			EditorGUILayout.LabelField ("3) Move left");
			EditorGUILayout.LabelField ("4) Move right");

			var targetController = (CharacterMovementsController)target;
			targetController.FillKeys (4);
			DrawDefaultInspector ();
		}
	}
}