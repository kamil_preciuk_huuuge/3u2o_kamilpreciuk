﻿using UDProject.Movements;
using UnityEditor;

namespace UDProject.EditorUtility
{
	[CustomEditor(typeof(CharacterRotatingController))]
	public class CharacterRotatingControllerInspector : Editor
	{
		private const int KEYS_LENGTH = 2;

		public override void OnInspectorGUI()
		{
			EditorGUILayout.LabelField ("1) Rotate left");
			EditorGUILayout.LabelField ("2) Rotate right");

			var targetController = (CharacterRotatingController)target;
			targetController.FillKeys (2);
			DrawDefaultInspector ();
		}
	}
}