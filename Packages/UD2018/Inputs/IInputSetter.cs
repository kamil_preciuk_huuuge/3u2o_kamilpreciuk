﻿using UnityEngine;
using System.Collections.Generic;

namespace UDProject.Inputs
{
	public interface IInputSetter 
	{
		Dictionary<KeyCode, bool> InputFlags { get; set; }
		void SetInputController(List<KeyCode> keys);
	}
}