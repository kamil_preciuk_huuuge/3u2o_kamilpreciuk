﻿using UnityEngine;
using System.Collections.Generic;

namespace UDProject.Inputs
{
	public abstract class StandardInputController : MonoBehaviour, IInputSetter
	{
		public List<KeyCode> keys;
		[HideInInspector] public bool isInputControllerActive = false;
		public Dictionary<KeyCode, bool> InputFlags { get; set; }

		public void SetInputController (List<KeyCode> keys)
		{
			isInputControllerActive = true;
			PrepareInputKeys (keys);
		}

		private void PrepareInputKeys(List<KeyCode> keys)
		{
			InputFlags = new Dictionary<KeyCode, bool> ();
			foreach (var key in keys)
			{
				InputFlags [key] = false;
			}
		}

		void Awake()
		{
			SetInputController (keys);
			OnAwake ();
		}

		protected abstract void OnAwake();

		void Update()
		{
			foreach (var key in keys)
			{
				InputFlags [key] = Input.GetKey (key);
			}
			OnUpdate ();
		}

		protected abstract void OnUpdate ();
	}
}

