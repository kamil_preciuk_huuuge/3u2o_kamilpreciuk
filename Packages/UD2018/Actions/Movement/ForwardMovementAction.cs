using UnityEngine;

namespace UDProject.Actions.Movement
{
    public class ForwardMovementAction : VectorMovementAction
    {
        public override void MakeAction()
        {
            GoByVector(GetTargetOffset());
        }

        public override void MakeActionInteger(int value)
        {
            GoByVector(GetTargetOffset() * value);
        }

        Vector3 GetTargetOffset()
        {
            var forward = transform.forward;
            return new Vector3
            {
                x = forward.x * defaultOffset.x,
                y = forward.y * defaultOffset.y,
                z = forward.z * defaultOffset.z
            };
        }
    }
}