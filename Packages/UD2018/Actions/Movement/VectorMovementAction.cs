using UnityEngine;

namespace UDProject.Actions.Movement
{
    public class VectorMovementAction : StandardGameAction
    {
        [SerializeField] protected Vector3 defaultOffset;
        
        public override void MakeAction()
        {
            GoByVector(defaultOffset);
        }

        public override void MakeActionInteger(int value)
        {
            GoByVector(defaultOffset * value);
        }
        
        protected void GoByVector(Vector3 offset)
        {
            transform.Translate(offset * Time.deltaTime);
        }
    }
}