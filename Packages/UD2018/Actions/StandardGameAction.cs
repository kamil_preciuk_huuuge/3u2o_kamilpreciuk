﻿using UnityEngine;

namespace UDProject.Actions
{
	public abstract class StandardGameAction : MonoBehaviour, IGameAction
	{
		public abstract void MakeAction ();
		public abstract void MakeActionInteger (int value);
	}
}

