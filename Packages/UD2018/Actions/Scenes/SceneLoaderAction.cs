﻿using UnityEngine.SceneManagement;
using UnityEngine;

namespace UDProject.Actions.Scenes
{
	public class SceneLoaderAction : StandardGameAction
	{
		[SerializeField] private string targetSceneName;

		public override void MakeAction ()
		{
			SceneManager.LoadScene (targetSceneName);
		}

		public override void MakeActionInteger (int value)
		{
			var sceneId = value - 1;
			SceneManager.LoadScene (sceneId);
		}
	}
}

