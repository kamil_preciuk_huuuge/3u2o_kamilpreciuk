﻿using UnityEngine.SceneManagement;

namespace UDProject.Actions.Scenes
{
	public class SceneReloaderAction : StandardGameAction
	{
		public override void MakeAction ()
		{
			ReloadScene ();
		}

		public override void MakeActionInteger (int value)
		{
			ReloadScene ();
		}

		private void ReloadScene()
		{
			var currentSceneName = SceneManager.GetActiveScene ().name;
			SceneManager.LoadScene (currentSceneName);
		}
	}
}

