﻿using UnityEngine;

namespace UDProject.Actions.Objects
{
	public class ObjectActiveStatusAction : StandardGameAction
	{
		[SerializeField] private GameObject target;
		[SerializeField] private bool shouldEnable;
		[SerializeField] private bool oneTimeEnable;

		public override void MakeAction ()
		{
			SetActive ();
		}

		public override void MakeActionInteger (int value)
		{
			SetActive ();
		}

		private void SetActive()
		{
			target.SetActive (shouldEnable);
			if (oneTimeEnable)
			{
				Destroy (this);
			}
		}
	}
}

