﻿using UnityEngine;

namespace UDProject.Actions.Objects
{
	public class ObjectDestroyingAction : StandardGameAction
	{
		[SerializeField] private GameObject target;

		public override void MakeAction ()
		{
			DestroyObject ();
		}

		public override void MakeActionInteger (int value)
		{
			DestroyObject ();
		}

		private void DestroyObject()
		{
			Destroy (target);
			Destroy (this);
		}
	}
}

