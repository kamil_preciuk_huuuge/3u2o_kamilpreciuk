﻿using UnityEngine;

namespace UDProject.Actions.Objects
{
	public class BehaviourActiveStatusAction : StandardGameAction
	{
		[SerializeField] private MonoBehaviour target;
		[SerializeField] private bool shouldEnable;
		[SerializeField] private bool oneTimeEnable;

		public override void MakeAction ()
		{
			SetActive ();
		}

		public override void MakeActionInteger (int value)
		{
			SetActive ();
		}

		private void SetActive()
		{
			target.enabled = shouldEnable;
			if (oneTimeEnable)
			{
				Destroy (this);
			}
		}
	}
}

