﻿using UnityEngine;
using System.Collections;

namespace UDProject.Actions.Objects
{
	public class SpawningGameObjectAction : StandardGameAction
	{
		[SerializeField] private GameObject prefab;
		[SerializeField] private float delayInMultiSpawning;

		[SerializeField] private Transform[] spawningPoints;
		[SerializeField] private bool shouldRandomize;
		private int lastSpawningId = -1;

		public override void MakeAction ()
		{
			SpawnObject ();
		}

		public override void MakeActionInteger (int value)
		{
			StartCoroutine (SpawnMultipleObjects (value));
		}

		IEnumerator SpawnMultipleObjects(int spawnsCount)
		{
			int spawnedObjects = 0;
			while (spawnedObjects < spawnsCount)
			{
				SpawnObject ();
				spawnedObjects++;
				yield return new WaitForSeconds (delayInMultiSpawning);
			}
		}

		private void SpawnObject()
		{
			var spawningTransform = transform;
			if (spawningPoints != null && spawningPoints.Length > 0)
			{
				if (shouldRandomize) 
				{
					var index = Random.Range (0, spawningPoints.Length);
					if (index == lastSpawningId) 
					{
						index = (index + 1) % spawningPoints.Length;
					}
					lastSpawningId = index;
				} 
				else
				{
					lastSpawningId = (lastSpawningId + 1) % spawningPoints.Length;
				}
				spawningTransform = spawningPoints [lastSpawningId];
			}
			Instantiate (prefab, spawningTransform.position, spawningTransform.rotation);
		}
	}
}

