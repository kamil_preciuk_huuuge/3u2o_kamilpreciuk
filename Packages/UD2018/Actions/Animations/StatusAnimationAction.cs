﻿using UnityEngine;

namespace UDProject.Actions.Animations
{
	public class StatusAnimationAction : StandardAnimationAction
	{
		[SerializeField] private string flagName;
		[SerializeField] private bool shouldEnableFlag;

		protected override void ProcessAction ()
		{
			animator.SetBool (flagName, shouldEnableFlag);
		}
	}
}

