﻿using UnityEngine;

namespace UDProject.Actions.Animations
{
	public class TriggeringAnimationAction : StandardAnimationAction
	{
		[SerializeField] private string triggerName;

		protected override void ProcessAction ()
		{
			animator.SetTrigger (triggerName);
		}
	}
}

