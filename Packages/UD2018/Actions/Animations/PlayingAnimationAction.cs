﻿using UnityEngine;

namespace UDProject.Actions.Animations
{
	public class PlayingAnimationAction : StandardAnimationAction
	{
		[SerializeField] private string stateName;

		protected override void ProcessAction ()
		{
			animator.Play (stateName);
		}
	}
}

