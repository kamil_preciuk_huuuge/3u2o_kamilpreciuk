﻿using UnityEngine;

namespace UDProject.Actions.Animations
{
	public abstract class StandardAnimationAction : StandardGameAction
	{
		[SerializeField] protected Animator animator;

		void Awake()
		{
            if (animator == null)
            {
                animator = GetComponent<Animator> ();
            }
		}

		public override void MakeAction ()
		{
			ProcessAction ();
		}

		public override void MakeActionInteger (int value)
		{
			ProcessAction ();
		}

		protected abstract void ProcessAction();
	}
}

