﻿namespace UDProject.Actions
{
	public interface IGameAction
	{
		void MakeAction ();
		void MakeActionInteger (int value);
	}
}