﻿using UnityEngine;
using UnityEngine.UI;

namespace UDProject.Actions.Score
{
	public class ScorePointsAction : StandardGameAction
	{
		[SerializeField] private Text scoreField;

		void Awake()
		{
            if (scoreField == null)
            {
                scoreField = GetComponent<Text> ();
            }
		}

		public override void MakeAction ()
		{
			IncreaseScore (1);
		}

		public override void MakeActionInteger (int value)
		{
			IncreaseScore (value);
		}

		void IncreaseScore(int value)
		{
			var currentScore = 0;
			int.TryParse (scoreField.text, out currentScore);
			currentScore += value;
			scoreField.text = currentScore.ToString ("0");
		}
	}
}

