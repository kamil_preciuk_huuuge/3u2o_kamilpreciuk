using UnityEngine;
using System.Collections;

namespace UDProject.Actions.Processors
{
    public class ActionWithKeyConstantlyController : StandardActionController
    {
        [SerializeField] private KeyCode keyCode;

        void Update()
        {
            if (Input.GetKey(keyCode)) 
            {
                ProcessAction ();
            }
        }
    }
}

