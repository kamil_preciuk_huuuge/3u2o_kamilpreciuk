﻿using UnityEngine;
using System.Collections;

namespace UDProject.Actions.Processors
{
	public class ActionWithKeyController : StandardActionController
	{
		[SerializeField] private KeyCode keyCode;

		void Update()
		{
			if (Input.GetKeyDown (keyCode)) 
			{
				ProcessAction ();
			}
		}
	}
}

