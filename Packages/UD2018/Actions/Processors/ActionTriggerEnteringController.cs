﻿using UnityEngine;

namespace UDProject.Actions.Processors
{
	public class ActionTriggerEnteringController : StandardTriggerController 
	{
		void OnTriggerEnter(Collider other)
		{
			var targetObject = other.gameObject;
			TryProcessAction (targetObject);
		}

		void OnTriggerEnter2D(Collider2D other)
		{
			var targetObject = other.gameObject;
			TryProcessAction (targetObject);
		}
	}
}