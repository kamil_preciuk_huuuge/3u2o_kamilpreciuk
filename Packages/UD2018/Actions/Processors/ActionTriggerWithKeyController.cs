﻿using UnityEngine;

namespace UDProject.Actions.Processors
{
	public class ActionTriggerWithKeyController : StandardTriggerController 
	{
		[SerializeField] private KeyCode keyCode;

		void OnTriggerStay(Collider other)
		{
			var targetObject = other.gameObject;
			ProcessTriggerStay (targetObject);
		}

		void OnTriggerStay2D(Collider2D other)
		{
			var targetObject = other.gameObject;
			ProcessTriggerStay (targetObject);
		}

		private void ProcessTriggerStay(GameObject targetObject)
		{
			if (Input.GetKeyDown (keyCode)) 
			{
				ProcessAction ();
			}
		}
	}
}