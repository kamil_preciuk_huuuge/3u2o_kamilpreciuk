﻿using UnityEngine;

namespace UDProject.Actions.Processors
{
	public class ActionTriggerExitingController : StandardTriggerController
	{
		void OnTriggerExit(Collider other)
		{
			var targetObject = other.gameObject;
			TryProcessAction (targetObject);
		}

		void OnTriggerExit2D(Collider2D other)
		{
			var targetObject = other.gameObject;
			TryProcessAction (targetObject);
		}
	}
}

