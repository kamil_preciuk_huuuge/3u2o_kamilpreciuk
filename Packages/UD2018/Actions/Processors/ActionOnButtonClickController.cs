﻿using UnityEngine;
using UnityEngine.UI;

namespace UDProject.Actions.Processors
{
	public class ActionOnButtonClickController : StandardActionController
	{
        [SerializeField] Button button;

		void Awake()
		{
            if (button == null)
            {
                button = GetComponent<Button> ();
            }
			button.onClick.AddListener (ProcessAction);
		}

		void OnDestroy()
		{
			if (button != null)
			{
				button.onClick.RemoveListener (ProcessAction);
			}
		}
	}
}

