﻿using UnityEngine;
using System.Collections;

namespace UDProject.Actions.Processors
{
	public class ActionAfterTimeController : StandardActionController
	{
		[SerializeField] private float offset;
		[SerializeField] private float delay;

		void Start()
		{
			if (oneTimeTrigger)
			{
				StartCoroutine (PlaySingleAction ());
			}
			else
			{
				StartCoroutine (PlayActionLoop ());
			}
		}

		IEnumerator PlaySingleAction()
		{
			yield return new WaitForSeconds (offset + delay);
			ProcessAction ();
		}

		IEnumerator PlayActionLoop()
		{
			if (offset > 0.0f)
			{
				yield return new WaitForSeconds (offset);
			}
			while (true) 
			{
				yield return new WaitForSeconds (delay);
				ProcessAction ();
			}
		}
	}
}

