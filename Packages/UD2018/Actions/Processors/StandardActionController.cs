﻿using UnityEngine;
using System.Collections.Generic;

namespace UDProject.Actions.Processors
{
	public class StandardActionController : MonoBehaviour
	{
		[SerializeField] protected List<GameActionInfo> gameActions;
		[SerializeField] protected bool oneTimeTrigger;

		[System.Serializable]
		public class GameActionInfo
		{
			public StandardGameAction action;
			public int intValue;
		}

		protected void ProcessAction()
		{
			for (var i = 0; i < gameActions.Count; i++)
			{
				var actionInfo = gameActions[i];
				ProcessSingleAction (actionInfo.action, actionInfo.intValue);
			}
			if (oneTimeTrigger)
			{
				Destroy (this);
			}
		}

		private void ProcessSingleAction(StandardGameAction action, int intValue)
		{
			if (intValue != 0) 
			{
				action.MakeActionInteger (intValue);
			} 
			else 
			{
				action.MakeAction ();
			}

		}
	}
}

