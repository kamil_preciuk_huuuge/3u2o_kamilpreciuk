﻿using UnityEngine;
using UDProject.Actions;

namespace UDProject.Actions.Processors
{
	public class StandardTriggerController : StandardActionController
	{
		[SerializeField] private string targetTag;

		protected void TryProcessAction(GameObject targetObject)
		{
			if (CanProcess(targetObject)) 
			{
				ProcessAction ();
			}
		}

		protected bool CanProcess(GameObject targetObject)
		{
			return string.IsNullOrEmpty (targetTag) || targetObject.CompareTag (targetTag);
		}
	}
}

