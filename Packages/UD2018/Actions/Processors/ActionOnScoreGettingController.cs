﻿using UnityEngine;
using UnityEngine.UI;
using UDProject.Enums;

namespace UDProject.Actions.Processors
{
	public class ActionOnScoreGettingController : StandardActionController
	{
		[SerializeField] private StatementType statement;
		[SerializeField] private int scoreValue;

		[SerializeField] private Text scoreField;
		private string lastScoreValue;

		void Awake()
		{
            if (scoreField == null)
            {
                scoreField = GetComponent<Text> ();
            }
			lastScoreValue = scoreField.text;
		}

		void Update()
		{
			if (!scoreField.text.Equals (lastScoreValue))
			{
				lastScoreValue = scoreField.text;
				var scoreVal = int.Parse (lastScoreValue);
				if (HasCorrectScore (scoreVal)) 
				{
					ProcessAction ();
				}
			}
		}

		private bool HasCorrectScore(int score)
		{
			switch (statement)
			{
				case StatementType.Less:
					return score < scoreValue;
				case StatementType.LessOrEqual:
					return score <= scoreValue;
				case StatementType.Equal:
					return score == scoreValue;
				case StatementType.MoreOrEqual:
					return score >= scoreValue;
				case StatementType.More:
					return score > scoreValue;
			}
			return false;
		}
	}
}

