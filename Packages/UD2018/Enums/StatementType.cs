﻿namespace UDProject.Enums
{
	public enum StatementType
	{
		Less = 0,
		LessOrEqual,
		Equal,
		MoreOrEqual,
		More
	}
}