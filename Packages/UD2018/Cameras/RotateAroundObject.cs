﻿using UnityEngine;

namespace UDProject.Actions.Objects
{
	public class RotateAroundObject : MonoBehaviour
	{
		protected Vector3 lastMousePosition;
		[SerializeField] private Transform target;

		public float rotationSpeed;

		[SerializeField] private bool xAxisOn;
		[SerializeField] private bool yAxisOn;

		void Start()
		{
			lastMousePosition = Input.mousePosition;
		}

		void Update()
		{
			if (Input.GetMouseButton (0)) 
			{
				RotateAround ();
			}
			lastMousePosition = Input.mousePosition;
			transform.LookAt (target.position);
		}

		private void RotateAround()
		{
			var currentInput = (lastMousePosition - Input.mousePosition) * rotationSpeed * Time.deltaTime;
			if (xAxisOn) 
			{
				transform.RotateAround (target.position, Vector3.up, currentInput.x);
			}
			if (yAxisOn) 
			{
				transform.RotateAround (target.position, Vector3.right, currentInput.y);
			}
		}
	}
}