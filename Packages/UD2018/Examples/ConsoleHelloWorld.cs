﻿using UnityEngine;

namespace UDProject.Examples
{
	public class ConsoleHelloWorld : MonoBehaviour 
	{
		public string hello = "hello";
		[SerializeField] private string world = "world";
		[SerializeField] protected bool onSpaceKey;

		void Update()
		{
			if (ShouldPrintHelloWorld())
			{
				PrintHelloWorld ();
			}
		}

		private bool ShouldPrintHelloWorld()
		{
			return !onSpaceKey || Input.GetKeyUp (KeyCode.Space);
		}

		private void PrintHelloWorld()
		{
			Debug.Log (hello + " " + world);
		}
	}
}