﻿using UnityEngine;

namespace UDProject.Examples
{
	public class AdditionExample : MonoBehaviour
	{
		public int firstNumber;
		public int secondNumber;

		void Update()
		{
			Debug.Log (firstNumber + " + " + secondNumber + " = " + Add (firstNumber, secondNumber));
		}

		public int Add(int a, int b)
		{
			int c = a + b;
			return c;
		}
	}
}

