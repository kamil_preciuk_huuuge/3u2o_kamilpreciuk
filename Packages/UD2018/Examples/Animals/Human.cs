﻿using UnityEngine;

namespace UDProject.Examples.Animals
{
	public class Human : Animal
	{
		void Awake()
		{
			legsCount = 2;
			handsCount = 2;
		}

		public override void Eat (string foodName)
		{
			Debug.Log ("Human eating: " + foodName);
		}

		public override void MoveBy (Vector3 value)
		{
			base.MoveBy (value);
		}
	}
}

