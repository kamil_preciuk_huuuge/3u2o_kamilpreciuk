﻿using UnityEngine;

namespace UDProject.Examples.Animals
{
	public class Wolf : Animal
	{
		void Awake()
		{
			legsCount = 4;
			handsCount = 0;
		}

		public override void Eat (string foodName)
		{
			Debug.Log ("Wolf eating: " + foodName);
		}

		public override void MoveBy (Vector3 value)
		{
			base.MoveBy (value);
		}
	}
}

