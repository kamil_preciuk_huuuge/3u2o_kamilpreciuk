﻿using UnityEngine;

namespace UDProject.Examples.Animals
{
	public class Animal : MonoBehaviour
	{
		protected int legsCount = 4;
		public int handsCount = 0;
		private int eyesCount = 2;
		[SerializeField] protected float xSpeed; 
		private float ySpeed = default; 

		void Update()
		{
			if (Input.GetKeyUp (KeyCode.M))
			{
				MoveBy (new Vector3(xSpeed, ySpeed, 0.0f));
			}
			if (Input.GetKeyUp (KeyCode.E))
			{
				Eat ("Something");
			}
		}

		public virtual void Eat(string foodName)
		{
			Debug.Log ("EATING: "+foodName);
		}

		public virtual void MoveBy(Vector3 value)
		{
			Debug.Log ("MOVING BY: "+value);
		}
	}
}