﻿using UDProject.Inputs;
using UnityEngine;

namespace UDProject.Movements
{
	[RequireComponent(typeof(CharacterController))]
	public class CharacterMovementsController : StandardMovementsController
	{
		private CharacterController characterController;

		protected override void OnAwake ()
		{
			base.OnAwake ();
			characterController = GetComponent<CharacterController> ();
		}

		protected override void Move (Vector3 velocity)
		{
			characterController.Move (velocity);
		}
	}
}

