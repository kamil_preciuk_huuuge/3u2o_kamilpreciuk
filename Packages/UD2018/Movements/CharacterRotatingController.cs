﻿using UDProject.Inputs;
using UnityEngine;

namespace UDProject.Movements
{
	[RequireComponent(typeof(CharacterController))]
	public class CharacterRotatingController : StandardInputController
	{
		private Transform characterTransform;
		[SerializeField] private float rotationSpeed;

		protected override void OnAwake ()
		{
			characterTransform = transform;
		}

		protected override void OnUpdate ()
		{
			if (isInputControllerActive)
			{
				RotateCharacter ();
			}
		}

		void RotateCharacter()
		{
			var rorationVector = Vector3.zero;

			if (keys.Count > 0 && InputFlags [keys[0]])
			{
				RotateLeft (ref rorationVector);
			}
			if (keys.Count > 1 && InputFlags [keys[1]])
			{
				RotateRight (ref rorationVector);
			}

			characterTransform.Rotate (rorationVector);
		}

		void RotateLeft(ref Vector3 rorationVector)
		{
			rorationVector += new Vector3 (0.0f, -rotationSpeed * Time.deltaTime, 0.0f);
		}

		void RotateRight(ref Vector3 rorationVector)
		{
			rorationVector += new Vector3 (0.0f, rotationSpeed * Time.deltaTime, 0.0f);
		}
	}
}

