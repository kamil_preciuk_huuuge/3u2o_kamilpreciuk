﻿using UnityEngine;

namespace UDProject.Movements
{
	[RequireComponent(typeof(Rigidbody))]
	public class RigidbodyDirectionMovementController : MonoBehaviour, IMovementController
	{
		private Rigidbody targetRigidbody;
		[SerializeField] private Vector3 velocity;

		void Awake ()
		{
			targetRigidbody = GetComponent<Rigidbody> ();
		}

		void Update()
		{
			if (!Vector3.Equals (targetRigidbody.velocity, velocity))
			{
				targetRigidbody.velocity = velocity;
			}
		}
	}
}