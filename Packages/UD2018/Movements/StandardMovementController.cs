﻿using UDProject.Inputs;
using UnityEngine;

namespace UDProject.Movements
{
	public abstract class StandardMovementsController : StandardInputController, IMovementController
	{
		private Transform objectTransform;
		[SerializeField] private float forwardSpeed;
		[SerializeField] private float backwartSpeed;
		[SerializeField] private float axisSpeed;

		protected override void OnAwake ()
		{
			objectTransform = transform;
		}

		protected override void OnUpdate ()
		{
			if (isInputControllerActive)
			{
				var velocity = CalculateVelocity ();
				Move (velocity);
			}
		}

		Vector3 CalculateVelocity()
		{
			var movementVector = Vector3.zero;
			if (keys.Count > 0 && InputFlags[keys[0]]) 
			{
				CalculateForward (ref movementVector);
			}
			if (keys.Count > 1 && InputFlags [keys[1]])
			{
				CalculateBackward (ref movementVector);
			}
			if (keys.Count > 2 && InputFlags [keys[2]])
			{
				CalculateLeft (ref movementVector);
			}
			if (keys.Count > 3 && InputFlags [keys[3]])
			{
				CalculateRight (ref movementVector);
			}
			return movementVector;
		}

		void CalculateForward(ref Vector3 movementVector)
		{
			movementVector += objectTransform.forward * forwardSpeed * Time.deltaTime;
		}

		void CalculateBackward(ref Vector3 movementVector)
		{
			movementVector += -objectTransform.forward * backwartSpeed * Time.deltaTime;
		}

		void CalculateLeft(ref Vector3 movementVector)
		{
			movementVector += -objectTransform.right * axisSpeed * Time.deltaTime;
		}

		void CalculateRight(ref Vector3 movementVector)
		{
			movementVector += objectTransform.right * axisSpeed * Time.deltaTime;
		}

		protected abstract void Move(Vector3 velocity);
	}
}

