﻿using UDProject.Inputs;
using UnityEngine;

namespace UDProject.Movements
{
	[RequireComponent(typeof(Rigidbody))]
	public class RigidbodyMovementsController : StandardMovementsController
	{
		private Rigidbody targetRigidbody;

		protected override void OnAwake ()
		{
			base.OnAwake ();
			targetRigidbody = GetComponent<Rigidbody> ();
		}

		protected override void Move (Vector3 velocity)
		{
			targetRigidbody.AddForce (velocity);
		}
	}
}

