﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace UDProject.Movements
{
	[RequireComponent(typeof(CharacterController))]
	public class CharacterWaypointMovementController : MonoBehaviour, IMovementController
	{
		[SerializeField] private List<Transform> waypoints;
		[SerializeField] private bool shouldRandomize;
		[SerializeField] private float movementSpeed;
		[SerializeField] private float waitingTime;

		private CharacterController characterController;
		private Transform characterTransform;

		private int lastWaypointIdentifier = -1;

		void Awake()
		{
			characterController = GetComponent<CharacterController> ();
			characterTransform = transform;
		}

		void Start()
		{
			lastWaypointIdentifier = GetNextWaypointIdentifier ();
			StartCoroutine(MoveToWaypoint (waypoints [lastWaypointIdentifier]));
		}

		private int GetNextWaypointIdentifier()
		{
			if (shouldRandomize)
			{
				var newIdentifier = Random.Range (0, waypoints.Count);
				if (newIdentifier == lastWaypointIdentifier) 
				{
					newIdentifier = (newIdentifier + 1) % waypoints.Count;
				}
				return newIdentifier;
			}
			else 
			{
				return (lastWaypointIdentifier + 1) % waypoints.Count;
			}
		}

		private IEnumerator MoveToWaypoint(Transform waypoint)
		{
			if (waitingTime > 0.0f) 
			{
				yield return new WaitForSeconds (waitingTime);
			}

			var startPosition = characterTransform.position;
			var distance = Vector3.Distance (characterTransform.position, waypoint.position);
			characterTransform.LookAt (waypoint, characterTransform.up);
			var currentLerpValue = 0.0f;
			while (currentLerpValue < distance) 
			{
				yield return null;
				currentLerpValue = Mathf.Min(currentLerpValue + movementSpeed * Time.deltaTime, distance);
				characterController.Move (characterTransform.forward * movementSpeed * Time.deltaTime);
			}
			lastWaypointIdentifier = GetNextWaypointIdentifier ();
			StartCoroutine(MoveToWaypoint (waypoints [lastWaypointIdentifier]));
		}
	}
}