﻿using UnityEngine;

namespace UDProject.Movements
{
	public class AutoRotatingController : MonoBehaviour, IMovementController
	{
		[SerializeField] private Vector3 rotation;
		[SerializeField] private bool additiveRotation;
		private Transform objectTransform;

		void Awake()
		{
			objectTransform = transform;
		}

		void Update()
		{
			if (additiveRotation) 
			{
				AddRotation ();
			}
			else
			{
				SetRotation ();
			}
		}

		private void AddRotation()
		{
			objectTransform.Rotate (rotation * Time.deltaTime);
		}

		private void SetRotation()
		{
			if (!Vector3.Equals (objectTransform.rotation.eulerAngles, rotation)) 
			{
				objectTransform.rotation = Quaternion.Euler (rotation);
			}
		}
	}
}

