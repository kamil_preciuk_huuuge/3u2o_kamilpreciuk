﻿using UnityEngine;

namespace UDProject.Movements
{
	[RequireComponent(typeof(CharacterController))]
	public class CharacterDirectionMovementController : MonoBehaviour, IMovementController
	{
		[SerializeField] private Vector3 velocity;
		[SerializeField] private bool includeRotation;

		private CharacterController characterController;
		private Transform characterTransform;

		void Awake()
		{
			characterController = GetComponent<CharacterController> ();
			characterTransform = transform;
		}

		void Update()
		{
			if (includeRotation) 
			{
				var currentVelocity = characterTransform.TransformDirection (velocity);
				characterController.Move (currentVelocity * Time.deltaTime);
			}
			else
			{
				characterController.Move (velocity * Time.deltaTime);
			}

		}
	}
}

